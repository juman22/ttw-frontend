import Vue from 'vue';
import App from './App.vue';
import { buildRouter } from './router';
import store from './store';
import firebase from 'firebase';

const firebaseConfig = {
  apiKey: 'AIzaSyDCcxEX7DNMdcvxdS2gtHNIq7TAfYO-GV4',
  authDomain: 'table-top-world.firebaseapp.com',
  projectId: 'table-top-world',
  storageBucket: 'table-top-world.appspot.com',
  messagingSenderId: '1037910862009',
  appId: '1:1037910862009:web:cc90741b1f672add429368',
  measurementId: 'G-YB7SS0JL04'
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

const router = buildRouter(store);

Vue.config.productionTip = false;

firebase.auth().onAuthStateChanged(async (user) => {
  if (!user) {
    router.push('/login');
    let app;
    app = new Vue({
      router,
      store,
      render: (h) => h(App)
    }).$mount('#app');
    return;
  }
  await store.dispatch('userModule/setAuthData', user);
  const localUser = store.getters['userModule/currentUser'];
  if (!localUser || localUser.uid !== user?.uid) {
    await store.dispatch('userModule/fetchUserData', user?.uid);
  }
  console.log('AUTH STATE CHANGED');
  let app;
  app = new Vue({
    router,
    store,
    render: (h) => h(App)
  }).$mount('#app');
});
