import { Character } from '@/models/Character';
import firebase from 'firebase';
import Vue from 'vue';
import VueRouter, { RouterMode } from 'vue-router';
import { Store } from 'vuex';
import { RootState } from '../store/types';

Vue.use(VueRouter);

const mode: RouterMode = 'history';

export function buildRouter(store: Store<RootState>): VueRouter {
  const router = new VueRouter({
    mode: mode,
    routes: [
      {
        path: '/',
        name: 'CharacterList',
        component: () =>
          import(
            /* webpackChunkName: "settings" */ '../components/CharacterList.vue'
          ),
        meta: {
          requiresAuth: true
        },
        beforeEnter: async (to, from, next) => {
          const user = await store.getters['userModule/currentUser'];
          await store.dispatch(
            'characterModule/fetchCharactersByUserId',
            user.id
          );
          next();
        }
      },
      {
        path: '/login',
        name: 'Login',
        component: () =>
          import(/* webpackChunkName: "login" */ '../components/Login.vue')
      },
      {
        path: '/logout',
        name: 'Logout',
        beforeEnter: async (to, from, next) => {
          await store.dispatch('userModule/logout');
          next('/login');
        }
      },
      {
        path: '/register',
        name: 'Register',
        component: () =>
          import(
            /* webpackChunkName: "register" */ '../components/Register.vue'
          )
      },
      {
        path: '/settings',
        name: 'Settings',
        component: () =>
          import(
            /* webpackChunkName: "settings" */ '../components/SettingList.vue'
          ),
        meta: {
          requiresAuth: true
        },
        beforeEnter: async (to, from, next) => {
          await store.dispatch('settingModule/fetchAllSettings');
          next();
        }
      },
      {
        path: '/createCharacter',
        name: 'CreateCharacter',
        component: () =>
          import(
            /* webpackChunkName: "settings" */ '../components/CharacterForm.vue'
          ),
        meta: {
          requiresAuth: true
        },
        beforeEnter: async (to, from, next) => {
          await store.dispatch('settingModule/fetchAllSettings');
          next();
        }
      },
      {
        path: '/createEquipment',
        name: 'CreateEquipment',
        component: () =>
          import(
            /* webpackChunkName: "settings" */ '../components/EquipmentForm.vue'
          ),
        meta: {
          requiresAuth: true
        },
        beforeEnter: async (to, from, next) => {
          await store.dispatch('settingModule/fetchAllSettings');
          next();
        }
      },
      {
        path: '/createStatusEffect',
        name: 'CreateStatusEffect',
        component: () =>
          import(
            /* webpackChunkName: "settings" */ '../components/StatusEffectForm.vue'
          ),
        meta: {
          requiresAuth: true
        },
        beforeEnter: async (to, from, next) => {
          await store.dispatch('settingModule/fetchAllSettings');
          next();
        }
      },
      {
        path: '/createItem',
        name: 'CreateItem',
        component: () =>
          import(
            /* webpackChunkName: "settings" */ '../components/ItemForm.vue'
          ),
        meta: {
          requiresAuth: true
        },
        beforeEnter: async (to, from, next) => {
          await store.dispatch('settingModule/fetchAllSettings');
          next();
        }
      },
      {
        path: '/play/:id',
        name: 'Play',
        component: () =>
          import(/* webpackChunkName: "settings" */ '../components/Play.vue'),
        meta: {
          requiresAuth: true
        },
        async beforeEnter(to, from, next) {
          const characterId = to.params.id;
          await store.dispatch(
            'characterModule/fetchCurrentCharacter',
            characterId
          );
          await store.dispatch(
            'characterModule/fetchStatsByCurrentCharacter',
            characterId
          );
          await store.dispatch(
            'characterModule/fetchSkillsByCurrentCharacter',
            characterId
          );
          next();
        }
      }
    ]
  });

  router.beforeEach((to, from, next) => {
    const requiresAuth = to.matched.some((x) => x.meta.requiresAuth);

    console.log(`${JSON.stringify(firebase.auth().currentUser)}`);
    if (
      requiresAuth &&
      (!firebase.auth().currentUser || !store.getters['userModule/currentUser'])
    ) {
      next('/login');
    } else {
      next();
    }
  });

  return router;
}
