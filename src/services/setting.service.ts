import { Class } from '@/models/Class';
import { Equipment } from '@/models/Equipment';
import { Item } from '@/models/Item';
import { Race } from '@/models/Race';
import Setting from '@/models/Setting';
import { Skill } from '@/models/Skill';
import { Stat } from '@/models/Stat';
import { StatusEffect } from '@/models/StatusEffect';
import { AxiosService } from './axios.service';

export class SettingService {
  private readonly settingUri = 'setting';
  private readonly axiosService: AxiosService;

  constructor(axiosService: AxiosService) {
    this.axiosService = axiosService;
  }

  async fetchAllSettings(): Promise<Setting[]> {
    return this.axiosService.get<Setting[]>(undefined, this.settingUri);
  }

  async fetchRacesBySetting(settingId: number): Promise<Race[]> {
    return this.axiosService.get<Race[]>(undefined, `${this.settingUri}/race`, {
      settingId: settingId
    });
  }

  async fetchClassesBySetting(settingId: number): Promise<Class[]> {
    return this.axiosService.get<Class[]>(
      undefined,
      `${this.settingUri}/class`,
      {
        settingId: settingId
      }
    );
  }

  async fetchSkillsBySetting(settingId: number): Promise<Skill[]> {
    return this.axiosService.get<Skill[]>(
      undefined,
      `${this.settingUri}/skill`,
      {
        settingId: settingId
      }
    );
  }

  async fetchStatsBySetting(settingId: number): Promise<Stat[]> {
    return this.axiosService.get<Stat[]>(undefined, `${this.settingUri}/stat`, {
      settingId: settingId
    });
  }

  async fetchEquipmentBySetting(settingId: number): Promise<Equipment[]> {
    return this.axiosService.get<Equipment[]>(
      undefined,
      `${this.settingUri}/equipment`,
      {
        settingId: settingId
      }
    );
  }

  async searchEquipmentBySettingId(
    settingId: number,
    searchString: string
  ): Promise<Equipment[]> {
    return this.axiosService.get<Equipment[]>(
      undefined,
      `${this.settingUri}/search/equipment`,
      {
        settingId: settingId,
        searchString: searchString
      }
    );
  }

  async searchStatusEffectsBySettingId(
    settingId: number,
    searchString: string
  ): Promise<StatusEffect[]> {
    return this.axiosService.get<StatusEffect[]>(
      undefined,
      `${this.settingUri}/search/statusEffects`,
      {
        settingId: settingId,
        searchString: searchString
      }
    );
  }

  async searchItemsBySettingId(
    settingId: number,
    searchString: string
  ): Promise<Item[]> {
    return this.axiosService.get<Item[]>(
      undefined,
      `${this.settingUri}/search/items`,
      {
        settingId: settingId,
        searchString: searchString
      }
    );
  }

  async fetchItemBySetting(settingId: number): Promise<Item[]> {
    return this.axiosService.get<Item[]>(undefined, `${this.settingUri}/item`, {
      settingId: settingId
    });
  }

  async fetchStatusEffectsBySetting(
    settingId: number
  ): Promise<StatusEffect[]> {
    return this.axiosService.get<StatusEffect[]>(
      undefined,
      `${this.settingUri}/statusEffect`,
      {
        settingId: settingId
      }
    );
  }

  async createItem(item: Item): Promise<Object> {
    return this.axiosService.post<Object>(
      undefined,
      `${this.settingUri}/item`,
      item
    );
  }

  async createEquipment(equipment: Equipment): Promise<Object> {
    return this.axiosService.post<Object>(
      undefined,
      `${this.settingUri}/equipment`,
      equipment
    );
  }

  async createStatusEffect(statusEffect: StatusEffect): Promise<Object> {
    return this.axiosService.post<Object>(
      undefined,
      `${this.settingUri}/statusEffect`,
      statusEffect
    );
  }
}
