import { DiceThresholds } from '@/DiceThresholds';
import { CharacterSkill } from '@/models/CharacterSkill';
import { Dice } from '@/models/Dice';

export class DiceFactory {
  public static buildModDice(characterSkill: CharacterSkill): Dice {
    let modDice: Dice = new Dice(-4);
    for (const experienceThresholdKey in DiceThresholds.THRESHOLDS) {
      const experienceThreshold = Number(experienceThresholdKey);
      console.log(`type : ${typeof experienceThreshold}`);

      if (typeof experienceThreshold !== 'number') {
        console.log(
          `Invalid dice key ${experienceThreshold} for skill ${JSON.stringify(
            characterSkill
          )}`
        );
        continue;
      }

      if (characterSkill.experience >= experienceThreshold) {
        modDice = DiceThresholds.THRESHOLDS[experienceThresholdKey];
      } else {
        break;
      }
    }

    return modDice;
  }
}
