import { Character } from '@/models/Character';
import { CharacterEquipment } from '@/models/CharacterEquipment';
import { CharacterItem } from '@/models/CharacterItem';
import { CharacterSkill } from '@/models/CharacterSkill';
import { CharacterStat } from '@/models/CharacterStat';
import { CharacterStatusEffect } from '@/models/CharacterStatusEffect';
import { AxiosService } from './axios.service';

export class CharacterService {
  private readonly characterUri = 'character';
  private readonly axiosService: AxiosService;

  constructor(axiosService: AxiosService) {
    this.axiosService = axiosService;
  }

  async getCharacter(characterId: number): Promise<Character> {
    return this.axiosService.get<Character>(undefined, `${this.characterUri}`, {
      characterId: characterId
    });
  }

  async getCharactersByUser(userId: number): Promise<Character[]> {
    return this.axiosService.get<Character[]>(
      undefined,
      `${this.characterUri}/user`,
      {
        userId: userId
      }
    );
  }

  async createCharacter(character: Character): Promise<{ id: number }> {
    return this.axiosService.post<{ id: number }>(
      undefined,
      `${this.characterUri}`,
      character
    );
  }

  async updateCharacter(character: Character): Promise<{ id: number }> {
    return this.axiosService.post<{ id: number }>(
      undefined,
      `${this.characterUri}/update`,
      character
    );
  }

  async insertSkills(
    characterSkills: CharacterSkill[]
  ): Promise<CharacterSkill[]> {
    return this.axiosService.post<CharacterSkill[]>(
      undefined,
      `${this.characterUri}/skill`,
      characterSkills
    );
  }

  async insertStats(characterStats: CharacterStat[]): Promise<CharacterStat[]> {
    return this.axiosService.post<CharacterStat[]>(
      undefined,
      `${this.characterUri}/stat`,
      characterStats
    );
  }

  async updateSkill(characterSkill: CharacterSkill): Promise<CharacterSkill> {
    return this.axiosService.post<CharacterSkill>(
      undefined,
      `${this.characterUri}/skill/update`,
      characterSkill
    );
  }

  insertItem(characterItem: CharacterItem): Promise<{ id: number }> {
    return this.axiosService.post<{ id: number }>(
      undefined,
      `${this.characterUri}/item`,
      characterItem
    );
  }

  updateItemQuantity(characterItem: CharacterItem): Promise<CharacterItem> {
    return this.axiosService.post<CharacterItem>(
      undefined,
      `${this.characterUri}/item/quantity`,
      characterItem
    );
  }

  removeItemById(characterItemId: number | undefined): Promise<Object> {
    return this.axiosService.delete<Object>(
      undefined,
      `${this.characterUri}/item`,
      {
        characterItemId: characterItemId
      }
    );
  }

  insertEquipment(
    characterEquipment: CharacterEquipment
  ): Promise<{ id: number }> {
    return this.axiosService.post<{ id: number }>(
      undefined,
      `${this.characterUri}/equipment`,
      characterEquipment
    );
  }

  equipEquipment(
    characterEquipment: CharacterEquipment
  ): Promise<CharacterEquipment> {
    return this.axiosService.post<CharacterEquipment>(
      undefined,
      `${this.characterUri}/equipment/equip`,
      characterEquipment
    );
  }

  unequipEquipment(
    characterEquipment: CharacterEquipment
  ): Promise<CharacterEquipment> {
    return this.axiosService.post<CharacterEquipment>(
      undefined,
      `${this.characterUri}/equipment/unequip`,
      characterEquipment
    );
  }

  removeEquipmentById(equipmentId: number | undefined): Promise<Object> {
    return this.axiosService.delete<Object>(
      undefined,
      `${this.characterUri}/equipment`,
      {
        equipmentId: equipmentId
      }
    );
  }

  async getStats(characterId: number): Promise<CharacterStat[]> {
    return this.axiosService.get<CharacterStat[]>(
      undefined,
      `${this.characterUri}/stat`,
      {
        characterId: characterId
      }
    );
  }

  async getSkills(characterId: number): Promise<CharacterSkill[]> {
    return this.axiosService.get<CharacterSkill[]>(
      undefined,
      `${this.characterUri}/skill`,
      {
        characterId: characterId
      }
    );
  }

  async getItems(characterId: number): Promise<CharacterItem[]> {
    return this.axiosService.get<CharacterItem[]>(
      undefined,
      `${this.characterUri}/item`,
      {
        characterId: characterId
      }
    );
  }

  async getEquipment(characterId: number): Promise<CharacterEquipment[]> {
    return this.axiosService.get<CharacterEquipment[]>(
      undefined,
      `${this.characterUri}/equipment`,
      {
        characterId: characterId
      }
    );
  }

  async getStatusEffects(
    characterId: number
  ): Promise<CharacterStatusEffect[]> {
    return this.axiosService.get<CharacterStatusEffect[]>(
      undefined,
      `${this.characterUri}/statusEffects`,
      {
        characterId: characterId
      }
    );
  }

  async removeStatusEffectById(
    characterStatusEffectId: number | undefined
  ): Promise<Object> {
    return this.axiosService.delete<Object>(
      undefined,
      `${this.characterUri}/statusEffects`,
      {
        characterStatusEffectId: characterStatusEffectId
      }
    );
  }

  async insertStatusEffect(
    characterStatusEffect: CharacterStatusEffect
  ): Promise<{ id: number }> {
    console.log(
      `inserting status effect: ${JSON.stringify(characterStatusEffect)}`
    );
    return this.axiosService.post<{ id: number }>(
      undefined,
      `${this.characterUri}/statusEffects`,
      characterStatusEffect
    );
  }
}
