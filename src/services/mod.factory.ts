import { CharacterEquipment } from '@/models/CharacterEquipment';
import { CharacterSkill } from '@/models/CharacterSkill';
import { CharacterStat } from '@/models/CharacterStat';
import { CharacterStatusEffect } from '@/models/CharacterStatusEffect';

export class ModFactory {
  public static buildModifier(characterStat: CharacterStat): number {
    const statValue = characterStat.value;
    if (statValue > 19) {
      return 4;
    }
    if (statValue > 17) {
      return 3;
    }
    if (statValue > 14) {
      return 2;
    }
    if (statValue > 11) {
      return 1;
    }
    if (statValue > 8) {
      return 0;
    }
    if (statValue > 5) {
      return -1;
    }
    if (statValue > 2) {
      return -2;
    }

    return -3;
  }

  public static buildStatModifier(
    charStat: CharacterStat,
    characterEquipment: CharacterEquipment[],
    characterStatusEffects: CharacterStatusEffect[]
  ): number {
    let modifierValue = 0;
    const equippedItems = characterEquipment.filter((equipment) => {
      return equipment.isEquipped;
    });
    console.log(
      `ALL ITEMS : ${JSON.stringify(
        characterEquipment
      )}, EquippedItem : ${JSON.stringify(equippedItems)}`
    );
    console.log(`Char stat: ${JSON.stringify(charStat)}`);
    equippedItems.forEach((characterEquipment) => {
      const statModifiers = characterEquipment.equipment.statModifiers.filter(
        (modifier) => {
          return modifier.stat.id == charStat.stat.id;
        }
      );
      console.log(`Stat modifiers1 : ${JSON.stringify(statModifiers)}`);
      statModifiers.forEach((modifier) => {
        modifierValue += modifier.value;
      });
    });

    characterStatusEffects.forEach((characterStatusEffect) => {
      const statModifiers = characterStatusEffect.statusEffect.statModifiers.filter(
        (modifier) => {
          return modifier.stat.id == charStat.stat.id;
        }
      );
      console.log(`Stat modifiers2 : ${JSON.stringify(statModifiers)}`);

      statModifiers.forEach((modifier) => {
        modifierValue += modifier.value;
      });
    });
    console.log(`Modifier value: ${modifierValue}`);
    return modifierValue;
  }

  public static buildStatModifierForSkill(
    charSkill: CharacterSkill,
    characterEquipment: CharacterEquipment[],
    characterStatusEffects: CharacterStatusEffect[]
  ): number {
    let modifierValue = 0;
    const equippedItems = characterEquipment.filter((equipment) => {
      return equipment.isEquipped;
    });
    equippedItems.forEach((characterEquipment) => {
      const statModifiers = characterEquipment.equipment.statModifiers.filter(
        (modifier) => {
          return modifier.stat.id == charSkill.skill.stat.id;
        }
      );
      statModifiers.forEach((modifier) => {
        modifierValue += modifier.value;
      });
    });

    characterStatusEffects.forEach((characterStatusEffect) => {
      const statModifiers = characterStatusEffect.statusEffect.statModifiers.filter(
        (modifier) => {
          return modifier.stat.id == charSkill.skill.stat.id;
        }
      );

      statModifiers.forEach((modifier) => {
        modifierValue += modifier.value;
      });
    });

    return modifierValue;
  }

  public static buildSkillModifierForSkill(
    charSkill: CharacterSkill,
    characterEquipment: CharacterEquipment[],
    characterStatusEffects: CharacterStatusEffect[]
  ): number {
    let modifierValue = 0;
    const equippedItems = characterEquipment.filter((equipment) => {
      return equipment.isEquipped;
    });
    equippedItems.forEach((characterEquipment) => {
      const skillModifiers = characterEquipment.equipment.skillModifiers.filter(
        (modifier) => {
          return modifier.skill.id == charSkill.skill.id;
        }
      );
      skillModifiers.forEach((modifier) => {
        modifierValue += modifier.value;
      });
    });

    characterStatusEffects.forEach((characterStatusEffect) => {
      const skillModifiers = characterStatusEffect.statusEffect.skillModifiers.filter(
        (modifier) => {
          return modifier.skill.id == charSkill.skill.id;
        }
      );

      skillModifiers.forEach((modifier) => {
        modifierValue += modifier.value;
      });
    });

    return modifierValue;
  }

  public static buildTotalCharacterSkillModifier(
    charSkill: CharacterSkill,
    characterEquipment: CharacterEquipment[],
    characterStatusEffects: CharacterStatusEffect[]
  ): number {
    let modifierValue = 0;

    const statModifier = this.buildStatModifierForSkill(
      charSkill,
      characterEquipment,
      characterStatusEffects
    );
    modifierValue += statModifier;

    const skillModifier = this.buildSkillModifierForSkill(
      charSkill,
      characterEquipment,
      characterStatusEffects
    );
    modifierValue += skillModifier;

    const universalModifier = this.buildUniversalModifier(
      characterEquipment,
      characterStatusEffects
    );
    modifierValue += universalModifier;

    return modifierValue;
  }

  public static buildOverdoseModifier(
    hp: number,
    characterEquipment: CharacterEquipment[],
    characterStatusEffects: CharacterStatusEffect[]
  ): number {
    let modifier = 0;
    const equippedItems = characterEquipment.filter((equipment) => {
      return equipment.isEquipped;
    });
    equippedItems.forEach((characterEquipment) => {
      modifier += characterEquipment.equipment.universalModifier.overdose;
    });

    characterStatusEffects.forEach((characterStatusEffect) => {
      modifier += characterStatusEffect.statusEffect.universalModifier.overdose;
    });

    const universalModifier = this.buildUniversalModifier(
      characterEquipment,
      characterStatusEffects
    );

    return Math.ceil(hp / 2) + modifier + universalModifier;
  }

  public static buildUniversalModifier(
    characterEquipment: CharacterEquipment[],
    characterStatusEffects: CharacterStatusEffect[]
  ): number {
    let modifier = 0;

    const equippedItems = characterEquipment.filter((equipment) => {
      return equipment.isEquipped;
    });
    equippedItems.forEach((characterEquipment) => {
      modifier += characterEquipment.equipment.universalModifier.universal;
    });

    characterStatusEffects.forEach((characterStatusEffect) => {
      modifier +=
        characterStatusEffect.statusEffect.universalModifier.universal;
    });

    return modifier;
  }

  public static buildArmorClassModifier(
    characterEquipment: CharacterEquipment[],
    characterStatusEffects: CharacterStatusEffect[]
  ): number {
    let modifier = 0;
    const equippedItems = characterEquipment.filter((equipment) => {
      return equipment.isEquipped;
    });
    equippedItems.forEach((characterEquipment) => {
      modifier += characterEquipment.equipment.universalModifier.armorClass;
    });

    characterStatusEffects.forEach((characterStatusEffect) => {
      modifier +=
        characterStatusEffect.statusEffect.universalModifier.armorClass;
    });
    return modifier;
  }

  public static buildMaxHpModifier(
    characterEquipment: CharacterEquipment[],
    characterStatusEffects: CharacterStatusEffect[]
  ): number {
    let modifier = 0;
    const equippedItems = characterEquipment.filter((equipment) => {
      return equipment.isEquipped;
    });
    equippedItems.forEach((characterEquipment) => {
      modifier += characterEquipment.equipment.universalModifier.hp;
    });

    characterStatusEffects.forEach((characterStatusEffect) => {
      modifier += characterStatusEffect.statusEffect.universalModifier.hp;
    });
    return modifier;
  }

  public static buildMovementSpeedModifier(
    characterEquipment: CharacterEquipment[],
    characterStatusEffects: CharacterStatusEffect[]
  ): number {
    let modifier = 0;
    const equippedItems = characterEquipment.filter((equipment) => {
      return equipment.isEquipped;
    });
    equippedItems.forEach((characterEquipment) => {
      modifier += characterEquipment.equipment.universalModifier.movementSpeed;
    });

    characterStatusEffects.forEach((characterStatusEffect) => {
      modifier +=
        characterStatusEffect.statusEffect.universalModifier.movementSpeed;
    });
    return modifier;
  }

  public static buildInitiativeModifier(charStats: CharacterStat[]): number {
    const dexterity = charStats.find((charStat) => {
      return charStat.stat.name === 'Dexterity';
    });
    const constition = charStats.find((charStat) => {
      return charStat.stat.name === 'Constitution';
    });
    const intelligence = charStats.find((charStat) => {
      return charStat.stat.name === 'Intelligence';
    });
    const wisdom = charStats.find((charStat) => {
      return charStat.stat.name === 'Wisdom';
    });

    if (!dexterity || !constition || !intelligence || !wisdom) {
      console.log('Armor class stats not detected');
      return 0;
    }

    const dexterityModifier = ModFactory.buildModifier(dexterity);
    const constitutionModifier = ModFactory.buildModifier(constition);
    const intelligenceModifier = ModFactory.buildModifier(intelligence);
    const wisdomModifier = ModFactory.buildModifier(wisdom);

    return Math.ceil(
      dexterityModifier +
        constitutionModifier * 0.5 +
        (intelligenceModifier + wisdomModifier) * 0.25
    );
  }
}
