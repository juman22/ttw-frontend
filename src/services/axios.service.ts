import store from '@/store';
import axios from 'axios';

export class AxiosService {
  private readonly baseUri: string;

  constructor(baseUri: string) {
    this.baseUri = baseUri;
  }

  public async post<T>(
    baseUri?: string,
    uri?: string,
    reqData?: any
  ): Promise<T> {
    uri = this.resolveUri(baseUri, uri);

    const headers = this.buildHeaders();
    console.log(`request data: ${JSON.stringify(reqData)}`);
    const { data } = await axios.post<T>(uri, reqData, {
      headers: headers
    });

    return data;
  }

  public async get<T>(
    baseUri?: string,
    uri?: string,
    params?: any
  ): Promise<T> {
    uri = this.resolveUri(baseUri, uri);

    const headers = this.buildHeaders();

    console.log(`PARAMS: ${JSON.stringify(params)}`);
    const { data } = await axios.get<T>(uri, {
      params: params,
      headers: headers
    });

    return data;
  }

  public async delete<T>(
    baseUri?: string,
    uri?: string,
    params?: any
  ): Promise<T> {
    uri = this.resolveUri(baseUri, uri);

    const headers = this.buildHeaders();

    console.log(`PARAMS: ${JSON.stringify(params)}`);
    const { data } = await axios.delete<T>(uri, {
      params: params,
      headers: headers
    });

    return data;
  }

  private resolveUri(baseUri?: string, uri?: string): string {
    console.log(`${uri}`);
    if (!baseUri) {
      baseUri = this.baseUri;
    }

    if (!uri) {
      uri = baseUri;
    } else {
      uri = `${baseUri}/${uri}`;
    }

    return uri;
  }

  private buildHeaders() {
    const authUser: { token: string; userId: string } =
      store.getters['userModule/authUser'];
    if (!authUser) {
      console.error('No auth user for authorization header, please login');
    }

    const headers = {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${authUser.token}`
    };
    console.log(`Header ${JSON.stringify(headers)}`);
    return headers;
  }
}
