import User from '@/models/auth/User';
import { AxiosService } from './axios.service';

export class UserService {
  private readonly userUri = 'user';
  private readonly axiosService: AxiosService;

  constructor(axiosService: AxiosService) {
    this.axiosService = axiosService;
  }

  // Returns insert id of user
  public async insertAccount(user: User) {
    return this.axiosService.post<Object>(undefined, this.userUri, user);
  }

  public async getAccount(uid: string) {
    return this.axiosService.get<User>(undefined, this.userUri, {
      uid: uid
    });
  }
}
