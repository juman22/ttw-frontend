import Vuex, { StoreOptions } from 'vuex';
import Vue from 'vue';
import { RootState } from './types';
import { create as createUserModule, name as userModuleName } from './user';
import {
  create as createSettingModule,
  name as settingModuleName
} from './setting';
import {
  create as createCharacterModule,
  name as characterModuleName
} from './character';
import { AxiosService } from '@/services/axios.service';

Vue.use(Vuex);

const apiUrl = 'http://tabletopworld.net:3000/api';

const axiosService = new AxiosService(apiUrl);
const userModule = createUserModule(axiosService);
const settingModule = createSettingModule(axiosService);
const characterModule = createCharacterModule(axiosService);

const storeOptions: StoreOptions<RootState> = {
  modules: {
    [userModuleName]: userModule,
    [settingModuleName]: settingModule,
    [characterModuleName]: characterModule
  }
};

const store = new Vuex.Store(storeOptions);

export default store;
