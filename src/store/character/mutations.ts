import { Character } from '@/models/Character';
import { CharacterEquipment } from '@/models/CharacterEquipment';
import { CharacterItem } from '@/models/CharacterItem';
import { CharacterSkill } from '@/models/CharacterSkill';
import { CharacterStat } from '@/models/CharacterStat';
import { CharacterStatusEffect } from '@/models/CharacterStatusEffect';
import { MutationTree } from 'vuex';
import { CharacterState } from './types';

class MutationKeys {
  public static SET_CHARACTERS = 'SET_CHARACTERS';
  public static SET_CURRENT_CHARACTER = 'SET_CURRENT_CHARACTER';
  public static SET_CURRENT_CHARACTER_STATS = 'SET_CURRENT_CHARACTER_STATS';
  public static SET_CURRENT_CHARACTER_SKILLS = 'SET_CURRENT_CHARACTER_SKILLS';
  public static SET_CURRENT_CHARACTER_EQUIPMENT =
    'SET_CURRENT_CHARACTER_EQUIPMENT';
  public static SET_CURRENT_CHARACTER_STATUS_EFFECTS =
    'SET_CURRENT_CHARACTER_STATUS_EFFECTS';
  public static SET_CURRENT_CHARACTER_ITEMS = 'SET_CURRENT_CHARACTER_ITEMS';
}

const mutations: MutationTree<CharacterState> = {
  [MutationKeys.SET_CHARACTERS](state, characters: Character[]) {
    state.characters = characters;
  },
  [MutationKeys.SET_CURRENT_CHARACTER](state, currentCharacter: Character) {
    state.currentCharacter = currentCharacter;
  },
  [MutationKeys.SET_CURRENT_CHARACTER_STATS](
    state,
    characterStats: CharacterStat[]
  ) {
    state.currentCharacterStats = characterStats;
  },
  [MutationKeys.SET_CURRENT_CHARACTER_SKILLS](
    state,
    characterSkills: CharacterSkill[]
  ) {
    state.currentCharacterSkills = characterSkills;
  },
  [MutationKeys.SET_CURRENT_CHARACTER_EQUIPMENT](
    state,
    characterEquipment: CharacterEquipment[]
  ) {
    state.currentCharacterEquipment = characterEquipment;
  },
  [MutationKeys.SET_CURRENT_CHARACTER_STATUS_EFFECTS](
    state,
    characterStatusEffects: CharacterStatusEffect[]
  ) {
    state.currentCharacterStatusEffects = characterStatusEffects;
  },
  [MutationKeys.SET_CURRENT_CHARACTER_ITEMS](
    state,
    characterItems: CharacterItem[]
  ) {
    state.currentCharacterItems = characterItems;
  }
};

export { MutationKeys, mutations };
