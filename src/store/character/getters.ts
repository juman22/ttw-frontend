import { Character } from '@/models/Character';
import { CharacterEquipment } from '@/models/CharacterEquipment';
import { CharacterItem } from '@/models/CharacterItem';
import { CharacterSkill } from '@/models/CharacterSkill';
import { CharacterStat } from '@/models/CharacterStat';
import { CharacterStatusEffect } from '@/models/CharacterStatusEffect';
import { GetterTree } from 'vuex';
import { RootState } from '../types';
import { CharacterState } from './types';

export const getters: GetterTree<CharacterState, RootState> = {
  characters(state): Character[] {
    return state.characters;
  },
  currentCharacter(state): Character | null {
    return state.currentCharacter;
  },
  currentCharacterStats(state): CharacterStat[] {
    return state.currentCharacterStats;
  },
  currentCharacterSkills(state): CharacterSkill[] {
    return state.currentCharacterSkills;
  },
  currentCharacterEquipment(state): CharacterEquipment[] {
    return state.currentCharacterEquipment;
  },
  currentCharacterStatusEffects(state): CharacterStatusEffect[] {
    return state.currentCharacterStatusEffects;
  },
  currentCharacterItems(state): CharacterItem[] {
    return state.currentCharacterItems;
  }
};
