import { Character } from '@/models/Character';
import { CharacterEquipment } from '@/models/CharacterEquipment';
import { CharacterItem } from '@/models/CharacterItem';
import { CharacterSkill } from '@/models/CharacterSkill';
import { CharacterStat } from '@/models/CharacterStat';
import { CharacterStatusEffect } from '@/models/CharacterStatusEffect';
import { CharacterService } from '@/services/character.service';
import { ActionTree } from 'vuex';
import store from '..';
import { RootState } from '../types';
import { MutationKeys } from './mutations';
import { CharacterState } from './types';

export function createActions(
  characterService: CharacterService
): ActionTree<CharacterState, RootState> {
  const actions: ActionTree<CharacterState, RootState> = {
    async fetchCurrentCharacter({ commit }, characterId: number) {
      const character = await characterService.getCharacter(characterId);
      console.log(`${JSON.stringify(character)}`);
      commit(MutationKeys.SET_CURRENT_CHARACTER, character);
      return character;
    },
    async fetchCharactersByUserId({ commit }, userId: number) {
      const characters = await characterService.getCharactersByUser(userId);
      console.log(`${JSON.stringify(characters)}`);
      commit(MutationKeys.SET_CHARACTERS, characters);
    },
    async createCharacter({ commit }, character: Character) {
      return await characterService.createCharacter(character);
    },
    async updateCharacter({ commit }, character: Character) {
      return await characterService.updateCharacter(character);
    },
    async insertSkills({ commit }, characterSkills: CharacterSkill[]) {
      return await characterService.insertSkills(characterSkills);
    },
    async insertStats({ commit }, characterStats: CharacterStat[]) {
      return await characterService.insertStats(characterStats);
    },
    async updateSkill({ commit }, characterSkill: CharacterSkill) {
      return await characterService.updateSkill(characterSkill);
    },
    async insertCharacterItem({ commit }, characterItem: CharacterItem) {
      await characterService.insertItem(characterItem);
      const characterId = await store.getters[
        'characterModule/currentCharacter'
      ].id;
      await store.dispatch(
        'characterModule/fetchItemsByCurrentCharacter',
        characterId
      );
    },
    async updateCharacterItemQuantity(
      { commit },
      characterItem: CharacterItem
    ) {
      await characterService.updateItemQuantity(characterItem);
      const characterId = await store.getters[
        'characterModule/currentCharacter'
      ].id;
      await store.dispatch(
        'characterModule/fetchItemsByCurrentCharacter',
        characterId
      );
    },
    async removeItem({ commit }, characterItem: CharacterItem) {
      await characterService.removeItemById(characterItem.id);
      const characterId = await store.getters[
        'characterModule/currentCharacter'
      ].id;
      await store.dispatch(
        'characterModule/fetchItemsByCurrentCharacter',
        characterId
      );
    },
    async insertCharacterEquipment(
      { commit },
      characterEquipment: CharacterEquipment
    ) {
      await characterService.insertEquipment(characterEquipment);
      const characterId = await store.getters[
        'characterModule/currentCharacter'
      ].id;
      await store.dispatch(
        'characterModule/fetchEquipmentByCurrentCharacter',
        characterId
      );
    },
    async equipEquipment({ commit }, characterEquipment: CharacterEquipment) {
      await characterService.equipEquipment(characterEquipment);
      const characterId = await store.getters[
        'characterModule/currentCharacter'
      ].id;
      await store.dispatch(
        'characterModule/fetchEquipmentByCurrentCharacter',
        characterId
      );
    },
    async unequipEquipment({ commit }, characterEquipment: CharacterEquipment) {
      await characterService.unequipEquipment(characterEquipment);
      const characterId = await store.getters[
        'characterModule/currentCharacter'
      ].id;
      await store.dispatch(
        'characterModule/fetchEquipmentByCurrentCharacter',
        characterId
      );
    },
    async removeEquipment({ commit }, characterEquipment: CharacterEquipment) {
      await characterService.removeEquipmentById(characterEquipment.id);
      const characterId = await store.getters[
        'characterModule/currentCharacter'
      ].id;
      await store.dispatch(
        'characterModule/fetchEquipmentByCurrentCharacter',
        characterId
      );
    },
    async fetchStatsByCurrentCharacter({ commit }, characterId: number) {
      const stats = await characterService.getStats(characterId);
      console.log(`stats: ${JSON.stringify(stats)}`);
      commit(MutationKeys.SET_CURRENT_CHARACTER_STATS, stats);
      return stats;
    },
    async fetchSkillsByCurrentCharacter({ commit }, characterId: number) {
      const skills = await characterService.getSkills(characterId);
      console.log(`skills : ${JSON.stringify(skills)}`);
      commit(MutationKeys.SET_CURRENT_CHARACTER_SKILLS, skills);
      return skills;
    },
    async fetchItemsByCurrentCharacter({ commit }, characterId: number) {
      const items = await characterService.getItems(characterId);
      console.log(`items: : ${JSON.stringify(items)}`);
      commit(MutationKeys.SET_CURRENT_CHARACTER_ITEMS, items);
      return items;
    },
    async fetchEquipmentByCurrentCharacter({ commit }, characterId: number) {
      const equipment = await characterService.getEquipment(characterId);
      console.log(`equipment : ${JSON.stringify(equipment)}`);
      commit(MutationKeys.SET_CURRENT_CHARACTER_EQUIPMENT, equipment);
      return equipment;
    },
    async fetchStatusEffectsByCurrentCharacter(
      { commit },
      characterId: number
    ) {
      const statusEffects = await characterService.getStatusEffects(
        characterId
      );
      console.log(`statusEffects : ${JSON.stringify(statusEffects)}`);
      commit(MutationKeys.SET_CURRENT_CHARACTER_STATUS_EFFECTS, statusEffects);
      return statusEffects;
    },
    async removeStatusEffect({ commit }, statusEffect: CharacterStatusEffect) {
      await characterService.removeStatusEffectById(statusEffect.id);
      const characterId = await store.getters[
        'characterModule/currentCharacter'
      ].id;
      await store.dispatch(
        'characterModule/fetchStatusEffectsByCurrentCharacter',
        characterId
      );
    },
    async insertStatusEffect({ commit }, statusEffect: CharacterStatusEffect) {
      await characterService.insertStatusEffect(statusEffect);
      const characterId = await store.getters[
        'characterModule/currentCharacter'
      ].id;
      await store.dispatch(
        'characterModule/fetchStatusEffectsByCurrentCharacter',
        characterId
      );
    }
  };

  return actions;
}
