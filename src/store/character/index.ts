import { AxiosService } from '@/services/axios.service';
import { CharacterService } from '@/services/character.service';
import { Module } from 'vuex';
import { RootState } from '../types';
import { createActions } from './actions';
import { getters } from './getters';
import { mutations } from './mutations';
import { CharacterState } from './types';

export const state: CharacterState = {
  characters: [],
  currentCharacter: null,
  currentCharacterSkills: [],
  currentCharacterStats: [],
  currentCharacterEquipment: [],
  currentCharacterStatusEffects: [],
  currentCharacterItems: []
};

export function create(
  axiosService: AxiosService
): Module<CharacterState, RootState> {
  const characterService = new CharacterService(axiosService);
  const actions = createActions(characterService);
  const characterModule: Module<CharacterState, RootState> = {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
  };
  return characterModule;
}

export const name = 'characterModule';
