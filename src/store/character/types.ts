import { Character } from '@/models/Character';
import { CharacterEquipment } from '@/models/CharacterEquipment';
import { CharacterItem } from '@/models/CharacterItem';
import { CharacterSkill } from '@/models/CharacterSkill';
import { CharacterStat } from '@/models/CharacterStat';
import { CharacterStatusEffect } from '@/models/CharacterStatusEffect';

export interface CharacterState {
  characters: Character[];
  currentCharacter: Character | null;
  currentCharacterSkills: CharacterSkill[];
  currentCharacterStats: CharacterStat[];
  currentCharacterEquipment: CharacterEquipment[];
  currentCharacterStatusEffects: CharacterStatusEffect[];
  currentCharacterItems: CharacterItem[];
}
