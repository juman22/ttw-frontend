import { AxiosService } from '@/services/axios.service';
import { SettingService } from '@/services/setting.service';
import { Module } from 'vuex';
import { RootState } from '../types';
import { createActions } from './actions';
import { getters } from './getters';
import { mutations } from './mutations';
import { SettingState } from './types';

export const state: SettingState = {
  settings: [],
  currentSetting: null,
  races: [],
  classes: [],
  skills: [],
  stats: [],
  equipment: [],
  statusEffects: [],
  items: []
};

export function create(
  axiosService: AxiosService
): Module<SettingState, RootState> {
  const settingService = new SettingService(axiosService);
  const actions = createActions(settingService);
  const settingModule: Module<SettingState, RootState> = {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
  };
  return settingModule;
}

export const name = 'settingModule';
