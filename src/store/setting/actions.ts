import { Equipment } from '@/models/Equipment';
import { Item } from '@/models/Item';
import { StatusEffect } from '@/models/StatusEffect';
import { SettingService } from '@/services/setting.service';
import { ActionTree } from 'vuex';
import { RootState } from '../types';
import { MutationKeys } from './mutations';
import { SettingState } from './types';

export function createActions(
  settingService: SettingService
): ActionTree<SettingState, RootState> {
  const actions: ActionTree<SettingState, RootState> = {
    async fetchAllSettings({ commit }) {
      const settings = await settingService.fetchAllSettings();
      console.log(`${JSON.stringify(settings)}`);
      commit(MutationKeys.SET_SETTINGS, settings);
      return settings;
    },
    async fetchRaceBySettingId({ commit }, settingId: number) {
      const races = await settingService.fetchRacesBySetting(settingId);
      console.log(`${JSON.stringify(races)}`);
      commit(MutationKeys.SET_RACES, races);
      return races;
    },
    async fetchItemsBySettingId({ commit }, settingId: number) {
      const items = await settingService.fetchItemBySetting(settingId);
      console.log(`${JSON.stringify(items)}`);
      commit(MutationKeys.SET_ITEMS, items);
      return items;
    },
    async fetchClassBySettingId({ commit }, settingId: number) {
      const classes = await settingService.fetchClassesBySetting(settingId);
      console.log(`${JSON.stringify(classes)}`);
      commit(MutationKeys.SET_CLASSES, classes);
      return classes;
    },
    async fetchSkillsBySettingId({ commit }, settingId: number) {
      const skills = await settingService.fetchSkillsBySetting(settingId);
      console.log(`${JSON.stringify(skills)}`);
      commit(MutationKeys.SET_SKILLS, skills);
      return skills;
    },
    async fetchStatsBySettingId({ commit }, settingId: number) {
      const stats = await settingService.fetchStatsBySetting(settingId);
      console.log(`${JSON.stringify(stats)}`);
      commit(MutationKeys.SET_STATS, stats);
      return stats;
    },
    async fetchEquipmentBySettingId({ commit }, settingId: number) {
      const equipment = await settingService.fetchEquipmentBySetting(settingId);
      console.log(`${JSON.stringify(equipment)}`);
      commit(MutationKeys.SET_EQUIPMENT, equipment);
      return equipment;
    },
    async searchEquipmentBySettingId(
      { commit },
      data: { settingId: number; searchString: string }
    ): Promise<Equipment[]> {
      const equipment = await settingService.searchEquipmentBySettingId(
        data.settingId,
        data.searchString
      );
      console.log(`Search results: ${JSON.stringify(equipment)}`);
      return equipment;
    },
    async searchStatusEffectsBySettingId(
      { commit },
      data: { settingId: number; searchString: string }
    ): Promise<StatusEffect[]> {
      const statusEffects = await settingService.searchStatusEffectsBySettingId(
        data.settingId,
        data.searchString
      );
      console.log(`Search results: ${JSON.stringify(statusEffects)}`);
      return statusEffects;
    },
    async searchItemsBySettingId(
      { commit },
      data: { settingId: number; searchString: string }
    ): Promise<Item[]> {
      const items = await settingService.searchItemsBySettingId(
        data.settingId,
        data.searchString
      );
      console.log(`Search results: ${JSON.stringify(items)}`);
      return items;
    },
    async fetchStatusEffectsBySettingId({ commit }, settingId: number) {
      const statusEffects = await settingService.fetchStatusEffectsBySetting(
        settingId
      );
      console.log(`status effects: ${JSON.stringify(statusEffects)}`);
      commit(MutationKeys.SET_STATUS_EFFECTS, statusEffects);
      return statusEffects;
    },
    async createItem({ commit }, item: Item) {
      return await settingService.createItem(item);
    },
    async createEquipment({ commit }, equipment: Equipment) {
      return await settingService.createEquipment(equipment);
    },
    async createStatusEffect({ commit }, statusEffect: StatusEffect) {
      return await settingService.createStatusEffect(statusEffect);
    }
  };

  return actions;
}
