import { Class } from '@/models/Class';
import { Equipment } from '@/models/Equipment';
import { Item } from '@/models/Item';
import { Race } from '@/models/Race';
import Setting from '@/models/Setting';
import { Skill } from '@/models/Skill';
import { Stat } from '@/models/Stat';
import { StatusEffect } from '@/models/StatusEffect';
import { MutationTree } from 'vuex';
import { SettingState } from './types';

class MutationKeys {
  public static SET_SETTINGS = 'SET_SETTINGS';
  public static SET_CURRENT_SETTING = 'SET_CURRENT_SETTING';
  public static SET_RACES = 'SET_RACES';
  public static SET_CLASSES = 'SET_CLASSES';
  public static SET_SKILLS = 'SET_SKILLS';
  public static SET_STATS = 'SET_STATS';
  public static SET_EQUIPMENT = 'SET_EQUIPMENT';
  public static SET_STATUS_EFFECTS = 'SET_STATUS_EFFECTS';
  public static SET_ITEMS = 'SET_ITEMS';
}

const mutations: MutationTree<SettingState> = {
  [MutationKeys.SET_SETTINGS](state, settings: Setting[]) {
    state.settings = settings;
  },
  [MutationKeys.SET_CURRENT_SETTING](state, currentSetting: Setting) {
    state.currentSetting = currentSetting;
  },
  [MutationKeys.SET_RACES](state, races: Race[]) {
    state.races = races;
  },
  [MutationKeys.SET_CLASSES](state, classes: Class[]) {
    state.classes = classes;
  },
  [MutationKeys.SET_SKILLS](state, skills: Skill[]) {
    state.skills = skills;
  },
  [MutationKeys.SET_STATS](state, stats: Stat[]) {
    state.stats = stats;
  },
  [MutationKeys.SET_EQUIPMENT](state, equipment: Equipment[]) {
    state.equipment = equipment;
  },
  [MutationKeys.SET_STATUS_EFFECTS](state, statusEffects: StatusEffect[]) {
    state.statusEffects = statusEffects;
  },
  [MutationKeys.SET_ITEMS](state, items: Item[]) {
    state.items = items;
  }
};

export { MutationKeys, mutations };
