import { Class } from '@/models/Class';
import { Equipment } from '@/models/Equipment';
import { Item } from '@/models/Item';
import { Race } from '@/models/Race';
import Setting from '@/models/Setting';
import { Skill } from '@/models/Skill';
import { Stat } from '@/models/Stat';
import { StatusEffect } from '@/models/StatusEffect';
import { GetterTree } from 'vuex';
import { RootState } from '../types';
import { SettingState } from './types';

export const getters: GetterTree<SettingState, RootState> = {
  settings(state): Setting[] {
    return state.settings;
  },
  currentSetting(state): Setting | null {
    return state.currentSetting;
  },
  races(state): Race[] {
    return state.races;
  },
  classes(state): Class[] {
    return state.classes;
  },
  skills(state): Skill[] {
    return state.skills;
  },
  stats(state): Stat[] {
    return state.stats;
  },
  items(state): Item[] {
    return state.items;
  },
  equipment(state): Equipment[] {
    return state.equipment;
  },
  statusEffects(state): StatusEffect[] {
    return state.statusEffects;
  }
};
