import { Class } from '@/models/Class';
import { Equipment } from '@/models/Equipment';
import { Item } from '@/models/Item';
import { Race } from '@/models/Race';
import Setting from '@/models/Setting';
import { Skill } from '@/models/Skill';
import { Stat } from '@/models/Stat';
import { StatusEffect } from '@/models/StatusEffect';

export interface SettingState {
  settings: Setting[];
  currentSetting: Setting | null;
  races: Race[];
  classes: Class[];
  skills: Skill[];
  stats: Stat[];
  equipment: Equipment[];
  statusEffects: StatusEffect[];
  items: Item[];
}
