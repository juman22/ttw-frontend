import User from '@/models/auth/User';

export interface UserState {
  currentUser: User | null;
  authUser: { token: string; userId: string } | null;
  isLoggedIn: boolean;
}
