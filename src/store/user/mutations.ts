import User from '@/models/auth/User';
import { MutationTree } from 'vuex';
import { UserState } from './types';

class MutationKeys {
  public static SET_CURRENT_USER = 'SET_CURRENT_USER';
  public static SET_IS_LOGGED_IN = 'SET_IS_LOGGED_IN';
  public static SET_AUTH_USER = 'SET_AUTH_USER';
}

const mutations: MutationTree<UserState> = {
  [MutationKeys.SET_CURRENT_USER](state, user: User) {
    console.log(`USER: ${JSON.stringify(user)}`);
    state.currentUser = user;
  },
  [MutationKeys.SET_IS_LOGGED_IN](state, isLoggedIn: boolean) {
    state.isLoggedIn = isLoggedIn;
  },
  [MutationKeys.SET_AUTH_USER](
    state,
    authUser: { token: string; userId: string }
  ) {
    state.authUser = authUser;
  }
};

export { MutationKeys, mutations };
