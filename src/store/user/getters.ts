import User from '@/models/auth/User';
import { GetterTree } from 'vuex';
import { RootState } from '../types';
import { UserState } from './types';

export const getters: GetterTree<UserState, RootState> = {
  currentUser(state): User | null {
    return state.currentUser;
  },
  isLoggedIn(state): boolean {
    return state.isLoggedIn;
  },
  authUser(state): { token: string; userId: string } | null {
    console.log(`state : ${JSON.stringify(state)}`);
    return state.authUser;
  }
};
