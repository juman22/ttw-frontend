import User from '@/models/auth/User';
import { UserService } from '@/services/user.service';
import firebase from 'firebase';
import { ActionTree } from 'vuex';
import store from '..';
import { RootState } from '../types';
import { MutationKeys } from './mutations';
import { UserState } from './types';

export function createActions(
  userService: UserService
): ActionTree<UserState, RootState> {
  const actions: ActionTree<UserState, RootState> = {
    async login({ commit }, data: { email: string; password: string }) {
      await firebase
        .auth()
        .signInWithEmailAndPassword(data.email, data.password)
        .then(async (authUser) => {
          if (!authUser.user) {
            throw new Error('No user returned in auth request');
          }
          console.log(JSON.stringify(authUser.user));
          await store.dispatch('userModule/setAuthData', authUser.user);
          await store.dispatch('userModule/fetchUserData', authUser.user.uid);
        })
        .catch((error) => {
          console.log(error);
        });
    },
    async fetchUserData({ commit }, uid: string) {
      const user = await userService.getAccount(uid);
      console.log(`User: ${JSON.stringify(user)}`);
      commit(MutationKeys.SET_CURRENT_USER, user);
      commit(MutationKeys.SET_IS_LOGGED_IN, true);
    },
    async setAuthData({ commit }, user: firebase.User) {
      await user.getIdToken(true).then((idToken) => {
        console.log(`Token: ${idToken}`);
        const authId = user.uid;
        commit(MutationKeys.SET_AUTH_USER, { token: idToken, userId: authId });
      });
    },
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async register({ commit }, data: { user: User; password: string }) {
      firebase
        .auth()
        .createUserWithEmailAndPassword(data.user.email, data.password)
        .then(async (authUser) => {
          if (authUser.user) {
            data.user.uid = authUser.user.uid;
            await store.dispatch('userModule/setAuthData', authUser.user);
            await userService.insertAccount(data.user);
            alert('Successfully registered!');
          }
        })
        .catch((error) => {
          alert(error.message);
        });
    },
    async logout({ commit }) {
      await firebase.auth().signOut();
      commit(MutationKeys.SET_CURRENT_USER, null);
      commit(MutationKeys.SET_AUTH_USER, null);
      commit(MutationKeys.SET_IS_LOGGED_IN, false);
    }
  };

  return actions;
}
