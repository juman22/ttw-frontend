import { Module } from 'vuex';
import { RootState } from '../types';
import { createActions } from './actions';
import { UserState } from './types';
import { getters } from './getters';
import { mutations } from './mutations';
import { UserService } from '@/services/user.service';
import { AxiosService } from '@/services/axios.service';

export const state: UserState = {
  currentUser: null,
  isLoggedIn: false,
  authUser: null
};

export function create(
  axiosService: AxiosService
): Module<UserState, RootState> {
  const userService = new UserService(axiosService);
  const actions = createActions(userService);
  const userModule: Module<UserState, RootState> = {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
  };
  return userModule;
}

export const name = 'userModule';
