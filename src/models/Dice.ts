import { Chance } from 'chance';

export class Dice {
  private readonly diceSides: number;
  private readonly numDice: number;
  private readonly chance: Chance.Chance;

  constructor(diceSides: number, numDice = 1) {
    this.diceSides = diceSides;
    this.numDice = numDice;
    this.chance = new Chance();
  }

  public roll(): number {
    let roll = 0;
    const diceMax = Math.abs(this.diceSides);
    console.log(`${diceMax}, ${this.numDice}`);
    for (let i = 0; i < this.numDice; i++) {
      roll += this.chance.integer({ min: 1, max: diceMax });
    }
    return roll * Math.sign(this.diceSides);
  }

  public toString = (): string => {
    return `${this.numDice}d${this.diceSides}`;
  };

  public toStringNoSign = (): string => {
    return `${this.numDice}d${Math.abs(this.diceSides)}`;
  };

  public getSign = (): string => {
    const sign = Math.sign(this.diceSides);
    if (sign >= 0) {
      return '';
    } else {
      return '-';
    }
  };
}
