import { Character } from './Character';
import { Item } from './Item';

interface ICharacterItem {
  id: number | undefined;
  character: Character;
  item: Item;
  quantity: number;
}

export class CharacterItem implements ICharacterItem {
  id!: number | undefined;
  character!: Character;
  item!: Item;
  quantity!: number;
}
