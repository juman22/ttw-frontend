import Setting from './Setting';
import { Stat } from './Stat';

interface IStatModifier {
  id: number | undefined;
  setting: Setting;
  stat: Stat;
  value: number;
}

export class StatModifier implements IStatModifier {
  id: number | undefined;
  setting!: Setting;
  stat!: Stat;
  value!: number;
}
