interface IUniversalModifier {
  id: number | undefined;
  hp: number;
  armorClass: number;
  movementSpeed: number;
  overdose: number;
  universal: number;
}

export class UniversalModifier implements IUniversalModifier {
  id: number | undefined;
  hp!: number;
  armorClass!: number;
  movementSpeed!: number;
  overdose!: number;
  universal!: number;
}
