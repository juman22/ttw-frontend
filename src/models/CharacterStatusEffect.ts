import { Character } from './Character';
import { StatusEffect } from './StatusEffect';

interface ICharacterStatusEffect {
  id: number | undefined;
  character: Character;
  statusEffect: StatusEffect;
}

export class CharacterStatusEffect implements ICharacterStatusEffect {
  id!: number | undefined;
  character!: Character;
  statusEffect!: StatusEffect;
}
