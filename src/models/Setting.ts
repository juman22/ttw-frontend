interface ISetting {
  id: number;
  name: string;
}

class Setting implements ISetting {
  id!: number;
  name!: string;
}

export default Setting;
