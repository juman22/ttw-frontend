interface IUser {
  id: number;
  email: string;
  name: string;
  uid: string;
}

class User implements IUser {
  id!: number;
  email!: string;
  name!: string;
  uid!: string;
}

export default User;
