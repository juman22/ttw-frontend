import { StatModifier } from './StatModifier';
import Setting from './Setting';
import { SkillModifier } from './SkillModifier';
import { UniversalModifier } from './UniversalModifier';

interface IEquipment {
  id: number | undefined;
  name: string;
  notes: string;
  setting: Setting;
  universalModifier: UniversalModifier;
  statModifiers: StatModifier[];
  skillModifiers: SkillModifier[];
}

export class Equipment implements IEquipment {
  id: number | undefined;
  name!: string;
  notes!: string;
  setting!: Setting;
  universalModifier!: UniversalModifier;
  statModifiers!: StatModifier[];
  skillModifiers!: SkillModifier[];
}
