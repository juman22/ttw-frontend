import Setting from './Setting';

interface IItem {
  id: number | undefined;
  name: string;
  notes: string;
  setting: Setting;
}

export class Item implements IItem {
  id: number | undefined;
  name!: string;
  notes!: string;
  setting!: Setting;
}
