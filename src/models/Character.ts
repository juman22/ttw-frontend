import User from './auth/User';
import { Class } from './Class';
import { Race } from './Race';
import Setting from './Setting';

interface ICharacter {
  id: number | undefined;
  name: string;
  user: User | undefined;
  setting: Setting;
  race: Race;
  clazz: Class;
  hp: number;
  maxHp: number | undefined;
  endurantCharges: number;
  toxicity: number;
  hunger: number;
}

export class Character implements ICharacter {
  id!: number | undefined;
  name!: string;
  user!: User | undefined;
  setting!: Setting;
  race!: Race;
  clazz!: Class;
  hp!: number;
  maxHp!: number | undefined;
  endurantCharges!: number;
  toxicity!: number;
  hunger!: number;
}
