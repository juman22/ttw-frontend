import { Character } from './Character';
import { Equipment } from './Equipment';

interface ICharacterEquipment {
  id: number | undefined;
  character: Character;
  equipment: Equipment;
  isEquipped: boolean;
}

export class CharacterEquipment implements ICharacterEquipment {
  id!: number | undefined;
  character!: Character;
  equipment!: Equipment;
  isEquipped!: boolean;
}
