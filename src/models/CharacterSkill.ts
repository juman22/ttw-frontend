import { Character } from './Character';
import { Skill } from './Skill';

interface ICharacterSkill {
  id: number | undefined;
  skill: Skill;
  character: Character;
  experience: number;
}

export class CharacterSkill implements ICharacterSkill {
  id!: number | undefined;
  skill!: Skill;
  character!: Character;
  experience!: number;
}
