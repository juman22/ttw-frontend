import { Character } from './Character';
import { Stat } from './Stat';

interface ICharacterStat {
  id: number | undefined;
  stat: Stat;
  character: Character;
  value: number;
}

export class CharacterStat implements ICharacterStat {
  id!: number | undefined;
  stat!: Stat;
  character!: Character;
  value!: number;
}
