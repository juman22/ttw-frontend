import { Dice } from './Dice';

export class RollData {
  trigger!: number;
  dice!: Dice[];
  modifier!: number;
}
