import { StatModifier } from './StatModifier';
import Setting from './Setting';
import { UniversalModifier } from './UniversalModifier';
import { SkillModifier } from './SkillModifier';

interface IStatusEffect {
  id: number | undefined;
  name: string;
  setting: Setting;
  universalModifier: UniversalModifier;
  statModifiers: StatModifier[];
  skillModifiers: SkillModifier[];
}

export class StatusEffect implements IStatusEffect {
  id: number | undefined;
  name!: string;
  setting!: Setting;
  universalModifier!: UniversalModifier;
  statModifiers!: StatModifier[];
  skillModifiers!: SkillModifier[];
}
