import { Stat } from './Stat';

interface ISkill {
  id: number;
  name: string;
  stat: Stat;
}

export class Skill implements ISkill {
  id!: number;
  name!: string;
  stat!: Stat;
}
