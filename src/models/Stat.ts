interface IStat {
  id: number;
  name: string;
}

export class Stat implements IStat {
  id!: number;
  name!: string;
}
