import Setting from './Setting';
import { Skill } from './Skill';

interface ISkillModifier {
  id: number | undefined;
  setting: Setting;
  skill: Skill;
  value: number;
}

export class SkillModifier implements ISkillModifier {
  id: number | undefined;
  setting!: Setting;
  skill!: Skill;
  value!: number;
}
