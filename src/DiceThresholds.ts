import { Dice } from './models/Dice';

export class DiceThresholds {
  public static readonly THRESHOLDS: Record<number, Dice> = {
    0: new Dice(-4),
    80: new Dice(4),
    180: new Dice(6),
    320: new Dice(8),
    500: new Dice(10),
    720: new Dice(12),
    1000: new Dice(20)
  };
}
